<?php
  $themeName = '';
  $fontSize = '';
  // define all themes
  $themes = [
      'black-white-mode',
      'black-yellow-mode',
      'yellow-black-mode',
      'bright-mode',
      'dark-mode',
  ];
  // define all fonts
  $fonts = [
      'standard',
      'medium',
      'large',
      'xlarge',
  ];
  // for each theme
  foreach($themes as $theme) {
      // see the cookie
      if(isset($_COOKIE['theme']) && $_COOKIE['theme'] == $theme ) {
          $themeName = $theme;
      }
  }

  // for each theme
  foreach($fonts as $font) {
      // see the cookie
      if(isset($_COOKIE['font']) && $_COOKIE['font'] == $font ) {
          $fontSize = $font;
      }
  }
?>
  
  <body data-theme="<?php echo $themeName;?>" data-font="<?php echo $fontSize;?>" >